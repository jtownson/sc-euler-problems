import org.scalatest.{FlatSpec, Matchers}

class Problem7Spec extends FlatSpec with Matchers {

  "The 6th prime" should "be" in {
    Problem7.prime(6) should be (13)
  }

  "The 10001st prime" should "be" in {
    Problem7.prime(10001) should be(104743)
  }
}
