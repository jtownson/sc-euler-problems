import org.scalatest.{FlatSpec, Matchers}

class Problem1Spec extends FlatSpec with Matchers {
  "Sum" should "be correct" in {
    Problem1.sum(10) should be (23)
    Problem1.sum(1000) should be (233168)
  }
}
