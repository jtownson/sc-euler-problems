import org.scalatest.{FlatSpec, Matchers}

class Problem6Spec extends FlatSpec with Matchers {

  "Sum square difference of 1..10" should "be" in {
    Problem6.sumSquareDifference(1, 10) should be(2640)
  }

  "Sum square difference of 1..100" should "be" in {
    Problem6.sumSquareDifference(1, 100) should be(25164150)
  }
}
