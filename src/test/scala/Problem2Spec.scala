import org.scalatest.{FlatSpec, Matchers}

class Problem2Spec extends FlatSpec with Matchers {
  "first ten terms of fib" should "be" in {
    Problem2.fibonacciTerms(1) should be(Seq(1))
    Problem2.fibonacciTerms(2) should be(Seq(1, 2))
    Problem2.fibonacciTerms(10) should be(Seq(1, 2, 3, 5, 8, 13, 21, 34, 55, 89))
  }

  "the sum of the even-valued terms less than 4M" should "be" in {
    println(Problem2.termsNotExceeding(4000000).filter(i => i%2 == 0).sum)
  }
}
