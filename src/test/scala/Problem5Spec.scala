import org.scalatest.{FlatSpec, Matchers}

class Problem5Spec extends FlatSpec with Matchers {

  "for 4, 7, 12, 21, and 42 the smallest common multiple" should "be 84" in {
    Problem5.smallestMultiple(Seq(4, 7, 12, 21, 42)) should be(84)
  }

  "for the range 1 to 10 the smallest common multiple" should "be 2520" in {
    Problem5.smallestMultiple(1, 10) should be(2520)
  }

  "for the range 1 to 20 the smallest common multiple" should "be 232792560" in {
    Problem5.smallestMultiple(1, 20) should be(232792560)
  }
}
