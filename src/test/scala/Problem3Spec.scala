import org.scalatest.FunSpec

class Problem3Spec extends FunSpec {

  describe("PrimeFactors") {
    it("should get factors of 12") {
      assert(Problem3.primeFactors(12) == Seq(2, 2, 3))
    }
    it("should get the example case correct") {
      assert(Problem3.primeFactors(13195) == Seq(5, 7, 13, 29))
    }
    it("should get the biggest pf in example case correct") {
      assert(Problem3.primeFactors(13195).max == 29)
    }

    it("should get the largest pf of 600851475143") {
      assert(Problem3.primeFactors(600851475143L).max == 6857)
    }
  }
}
