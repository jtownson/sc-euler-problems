import org.scalatest.{FlatSpec, Matchers}

class Problem4Spec extends FlatSpec with Matchers {

  "The largest palindrome from the product of 2-digit numbers" should "be 9009" in {
    Problem4.largestPalindrome(2) should be(Palindrome(91, 99))
  }

  "The largest palindrome from the product of 3-digit numbers" should "be 1" in {
    Problem4.largestPalindrome(3) should be(Palindrome(913,993))
  }
}
