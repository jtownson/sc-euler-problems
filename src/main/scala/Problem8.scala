object Problem8 {

  def greatestProduct(seq: String, windowSize: Int): (String, BigInt) = {
    val windows = seq.sliding(windowSize).map(s => s.toCharArray).map(ca => ca.map(c => BigInt(c.asDigit))).toList
    val products = windows.map(window => window.product)
    val windowStrings: List[String] = windows.map(window => window.mkString(" × "))
    val sortedDetails: List[(String, BigInt)] = (windowStrings zip products).sortBy(_._2).reverse

    sortedDetails.head
  }
}
