
case class Palindrome(x: Int, y: Int) extends Ordered[Palindrome] {
  def value = x*y

  override def compare(that: Palindrome): Int = value compare that.value
}

object Problem4 {

  def largestPalindrome(digits: Int): Palindrome = {
    val palindromes: Stream[Palindrome] = for {
      x <- Stream.from(1).take(max(digits))
      y <- Stream.from(1).take(max(digits)); p = x*y; if isPalindrome(p)
    } yield Palindrome(x,y)
    palindromes.max
  }

  def isPalindrome(i: Int): Boolean = {
    val s = i.toString
    s == s.reverse
  }

  private def max(digits: Int): Int = Math.pow(10, digits).toInt - 1
}
