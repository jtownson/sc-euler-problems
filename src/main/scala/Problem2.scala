

object Problem2 {

  val fibSeq: Stream[Int] = 1 #:: 2 #:: fibSeq.zip(fibSeq.tail).map(p => p._1 + p._2)

  def fibonacciTerms(n: Int): Seq[Int] = fibSeq.take(n)

  def termsNotExceeding(max: Int): Seq[Int] = fibSeq.takeWhile(i => i <= max)
}
