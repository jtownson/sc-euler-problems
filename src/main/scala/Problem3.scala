object Problem3 {

  val naturals: Stream[Long] = Stream.cons(1L, naturals.map(_ + 1))

  val from3: Stream[Long] = naturals.drop(2)

  val primes: Stream[Long] = 2L #:: from3.filter((i: Long) => primes.takeWhile(j => j*j <= i).forall(k => i % k != 0)) // NB: forall is *true* for an empty stream!

  def primeFactors(n: Long): Seq[Long] = {
    def loop(factors: List[Long], m: Long, remainingPrimes: Stream[Long]): Seq[Long] = {
      val prime = remainingPrimes.head
      if (m % prime == 0) {
        val p = m / prime
        if (p == 1)
          prime :: factors
        else
          loop(prime :: factors, p, primes)
      } else {
        loop(factors, m, remainingPrimes.tail)
      }

    }
    loop(List(), n, primes).reverse
  }
}
