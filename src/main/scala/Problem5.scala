

object Problem5 {

  def smallestMultiple(numbers: Seq[Long]): Long = {
    val primes: Iterator[Long] = Problem3.primes.iterator
    val one = Seq.fill(numbers.length)(1L)

    def getFactors(prime: Long, x: Seq[Long]): List[Long] = {

      val nextCol: Seq[Long] = x.map(xi => if (xi % prime == 0) xi / prime else xi)

      if (nextCol == one)
        prime :: Nil
      else if (nextCol == x)
        getFactors(primes.next(), nextCol)
      else
        prime :: getFactors(prime, nextCol)
    }

    getFactors(primes.next(), numbers).foldLeft(1L)(_ * _)
  }

  def smallestMultiple(min: Int, max: Int): Long = smallestMultiple(Seq.range(min, max + 1))
}
