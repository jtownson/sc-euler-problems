
object Problem1 {
  def sum(upperBound: Int): Int = Stream.from(1).take(upperBound-1).filter(i => i % 3 == 0 || i % 5 == 0).sum
}
