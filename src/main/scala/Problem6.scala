

object Problem6 {
  def sumSquareDifference(min: Int, max: Int): Long = squareOfSum(min, max) - sumOfSquares(min, max)

  val naturals: Stream[Long] = Stream.from(1).map(_.toLong)
  val squares: Stream[Long] = Stream.from(1).map(i => i.toLong*i.toLong)

  def sumOfSquares(min: Int, max: Int): Long = squares.slice(min-1, max).sum

  def squareOfSum(min: Int, max: Int): Long = Math.pow(naturals.slice(min-1, max).sum, 2).toLong
}
